package config

import (
	"io/ioutil"
	"log"
	"os"

	"encoding/json"
)

type Config struct {
	Bind      string
	DB        string
	CacheDir  string
	Host      string
	UserAgent string
}

var (
	DefaultConfig = Config{
		Bind:      "127.0.0.1:8080",
		DB:        "postgres://postgres:password@localhost/postgres?sslmode=disable",
		CacheDir:  "/tmp/podcache",
		Host:      "localhost",
		UserAgent: "Podcache <podcache@janky.solutions>",
	}
	ConfigFiles = []string{"/etc/podcast-proxy.json", "podcast-proxy.json"}
	C           Config
)

// Load loads the configuration off the disk
func Load() {
	C = DefaultConfig
	for _, filename := range ConfigFiles {
		jsonFile, err := os.Open(filename)
		if err != nil {
			if !os.IsNotExist(err) {
				log.Printf("Error opening config file %s: %s", filename, err.Error())
			}
			continue
		}
		defer jsonFile.Close()

		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			log.Printf("Error reading config file %s: %s", filename, err.Error())
			continue
		}

		json.Unmarshal(byteValue, &C)
		log.Println("Successfully read config from", filename)
	}
}
