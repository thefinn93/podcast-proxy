package main

import (
	"context"
	"fmt"
	"log"

	"github.com/alecthomas/kong"
	"github.com/kjk/betterguid"

	"gitlab.com/thefinn93/podcast-proxy/config"
	"gitlab.com/thefinn93/podcast-proxy/database"
)

type ListCommand struct{}
type AddCommand struct {
	URL   string `arg name:"url" help:"The URL of the new feed to add." type:"string"`
	Token string `arg optional name:"token" help:"The secret token to use. Generated otherwise"`
}

var CLI struct {
	List ListCommand `cmd help:"Lists all known feeds"`
	Add  AddCommand  `cmd help:"Add a new feed."`
}

func main() {
	log.Println("Loading podcast proxy...")
	config.Load()

	if err := database.Init(); err != nil {
		panic(err)
	}

	ctx := kong.Parse(&CLI)
	err := ctx.Run()
	ctx.FatalIfErrorf(err)
}

func (a *AddCommand) Run(ctx *kong.Context) error {
	if a.Token == "" {
		a.Token = betterguid.New()
	}
	log.Println("Adding new feed", a.URL, fmt.Sprintf("https://%s/%s", config.C.Host, a.Token))
	return database.DB.AddFeed(context.Background(), database.AddFeedParams{Token: a.Token, UpstreamUrl: a.URL})
}

func (l *ListCommand) Run(ctx *kong.Context) error {
	log.Println("Listing all known feeds")
	feeds, err := database.DB.ListFeeds(context.Background())
	if err != nil {
		return err
	}
	for _, r := range feeds {
		log.Println(r.Token, "\t", r.UpstreamUrl)
	}
	return nil
}
