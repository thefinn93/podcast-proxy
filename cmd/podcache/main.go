package main

import (
	"log"
	"os"

	"gitlab.com/thefinn93/podcast-proxy/config"
	"gitlab.com/thefinn93/podcast-proxy/database"
	"gitlab.com/thefinn93/podcast-proxy/podcache"
)

func main() {
	log.Println("Loading podcast proxy...")
	config.Load()

	os.MkdirAll(config.C.CacheDir, os.ModePerm)

	if err := database.Init(); err != nil {
		panic(err)
	}

	go podcache.PeriodicCleanup()

	podcache.ListenAndServe()
}
