package database

import (
	"database/sql"

	_ "github.com/lib/pq"

	"gitlab.com/thefinn93/podcast-proxy/config"
)

var DB Queries

func Init() error {
	db, err := sql.Open("postgres", config.C.DB)
	if err != nil {
		return err
	}

	DB = Queries{db: db}
	return nil
}
