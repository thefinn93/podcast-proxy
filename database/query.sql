-- name: GetMediaToken :one
SELECT token FROM media WHERE source_url = $1 LIMIT 1;

-- name: Download :one
UPDATE media SET last_download = NOW() WHERE token = $1 RETURNING *;

-- name: GetMediaSource :one
SELECT * FROM media WHERE token = $1 LIMIT 1;

-- name: SetMediaContentType :exec
UPDATE media SET content_type = $1 WHERE token = $2;

-- name: CreateMedia :exec
INSERT INTO media (token, source_url) VALUES ($1, $2);

-- name: GetOldMedia :many
SELECT token FROM media WHERE last_download < NOW() - INTERVAL '3 days' ORDER BY last_download ASC;

-- name: GetFeedUpstreamURL :one
SELECT upstream_url FROM feeds WHERE token = $1;

-- name: AddFeed :exec
INSERT INTO feeds (token, upstream_url) VALUES ($1, $2);

-- name: ListFeeds :many
SELECT * FROM feeds;
