CREATE TABLE media (
  token         text      NOT NULL PRIMARY KEY,
  source_url    text      NOT NULL,
  content_type  text,
  last_download timestamp
);

CREATE TABLE feeds (
  token        text NOT NULL PRIMARY KEY,
  upstream_url text NOT NULL
);
