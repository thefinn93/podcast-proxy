package podcache

import (
	"context"
	"log"
	"os"
	"time"

	"gitlab.com/thefinn93/podcast-proxy/database"
)

func PeriodicCleanup() {
	for {
		go DoCleanup()
		time.Sleep(1 * time.Hour)
	}
}

func DoCleanup() {
	media, err := database.DB.GetOldMedia(context.Background())
	if err != nil {
		log.Println("Error running cleanup of old media", err)
		return
	}

	for _, token := range media {
		filename := getCacheFilename(token)
		_, err := os.Stat(filename)

		if os.IsNotExist(err) {
			continue
		}

		log.Println("Deleting", filename)
		os.Remove(filename)
	}
}
