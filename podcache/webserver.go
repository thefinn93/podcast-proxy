package podcache

import (
	"context"
	"database/sql"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/beevik/etree"
	"github.com/gorilla/mux"
	"github.com/kjk/betterguid"

	"gitlab.com/thefinn93/podcast-proxy/config"
	"gitlab.com/thefinn93/podcast-proxy/database"
)

type MaybeWriter struct {
	Writer io.Writer
}

func (m MaybeWriter) Write(p []byte) (int, error) {
	n, err := m.Writer.Write(p)
	if err != nil {
		log.Println("Error writing to stream", err)
	}
	return n, nil
}

func ListenAndServe() {
	r := mux.NewRouter()
	r.HandleFunc("/{feed-token}", proxyFeed)
	r.HandleFunc("/download/{file-token}", download)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	r.Use(loggingMiddleware)
	log.Fatal(http.ListenAndServe(config.C.Bind, r))
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
		log.Println(r.RemoteAddr, r.Method, r.RequestURI)
	})
}

func proxyFeed(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token, ok := vars["feed-token"]
	if !ok {
		http.Error(w, "not found", 404)
		return
	}

	upstreamURL, err := database.DB.GetFeedUpstreamURL(r.Context(), token)
	if err == sql.ErrNoRows {
		http.Error(w, "not found", 404)
		return
	} else if err != nil {
		log.Println("Error getting feed from token:", err)
		http.Error(w, err.Error(), 500)
		return
	}

	upstream, err := upstreamRequest(upstreamURL)
	if err != nil {
		log.Println("Error making upstream request:", err)
		http.Error(w, err.Error(), 500)
		return
	}
	defer upstream.Body.Close()

	podcast := etree.NewDocument()
	if _, err := podcast.ReadFrom(upstream.Body); err != nil {
		log.Println("Error making parsing response from upstream:", err)
		http.Error(w, err.Error(), 500)
		return
	}

	replaceURLs(r.Context(), podcast, "//channel/item/enclosure", "url")
	replaceURLs(r.Context(), podcast, "//channel/itunes:image", "href")
	replaceURLs(r.Context(), podcast, "//channel/image/url", "")

	podcast.WriteTo(w)
}

func getProxyURL(ctx context.Context, url string) string {
	token, err := database.DB.GetMediaToken(ctx, url)
	if err != nil {
		token = betterguid.New()
		err = database.DB.CreateMedia(ctx, database.CreateMediaParams{SourceUrl: url, Token: token})
		if err != nil {
			log.Println("error storing new URL mapping:", err)
		}
	}
	return fmt.Sprintf("https://%s/download/%s", config.C.Host, token)
}

func download(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token, ok := vars["file-token"]
	if !ok {
		http.Error(w, "not found", 404)
		return
	}

	source, err := database.DB.Download(r.Context(), token)
	if err != nil {
		http.Error(w, err.Error(), 404)
		return
	}

	cacheFilename := getCacheFilename(token)
	if serveFromCache(w, cacheFilename, source.ContentType) {
		return
	}

	resp, err := upstreamRequest(source.SourceUrl)
	if err != nil {
		log.Println("error fetching remote file", source.SourceUrl, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// close the original resp.Body, even if we wrap it in a NopCloser below
	defer resp.Body.Close()

	contentType := resp.Header.Get("content-type")
	if contentType != "" {
		database.DB.SetMediaContentType(r.Context(), database.SetMediaContentTypeParams{ContentType: sql.NullString{String: contentType, Valid: true}, Token: token})
		w.Header().Set("Content-Type", contentType)
	}

	contentLength := resp.Header.Get("content-length")
	if contentLength != "" {
		w.Header().Set("Content-Length", contentLength)
	}

	cacheFile, err := os.OpenFile(cacheFilename, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Println("Error opening", cacheFilename, "for writing:", err.Error())
		log.Println("No caching will occur!")
		io.Copy(w, resp.Body)
		return
	}
	defer cacheFile.Close()

	log.Println("Caching", source.SourceUrl, "to", cacheFilename)
	io.Copy(cacheFile, io.TeeReader(resp.Body, MaybeWriter{Writer: w}))
}

func serveFromCache(w http.ResponseWriter, filename string, contentType sql.NullString) bool {
	fileinfo, err := os.Stat(filename)
	if os.IsNotExist(err) {
		log.Println("Cache miss:", filename)
		return false
	}

	if fileinfo.Size() == 0 {
		log.Println("File", filename, "is empty, so trying to re-download")
		return false
	}

	file, err := os.Open(filename)
	if err != nil {
		log.Println("Error opening cached file", filename, "for reading:", err)
		return false
	}

	if contentType.Valid {
		w.Header().Set("Content-Type", contentType.String)
	}

	w.Header().Set("Content-Length", string(fileinfo.Size()))

	io.Copy(w, file)
	return true
}

func upstreamRequest(url string) (resp *http.Response, err error) {
	log.Println("Proxying request to", url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("User-Agent", config.C.UserAgent)

	client := http.Client{}
	resp, err = client.Do(req)
	return
}

func replaceURLs(ctx context.Context, root *etree.Document, path string, attribute string) {
	for _, e := range root.FindElements(path) {
		if attribute == "" {
			e.SetText(getProxyURL(ctx, e.Text()))
			continue
		}

		attr := e.SelectAttr(attribute)
		if attr == nil || attr.Value == "" {
			continue
		}
		attr.Value = getProxyURL(ctx, attr.Value)
	}
}

func getCacheFilename(token string) string {
	return fmt.Sprintf("%s/%s", config.C.CacheDir, token)
}
